CAB302 Software Development
===========================

# Practical 9: The Integrated Build Process

The classes for this week are about integrated builds in the modern development environment.  Build systems like Ant
allow convenient automation of processes such as compilation, testing, documentation generation,  and deployment. We
begin slowly, but build up to generating a professional build file for some existing code. We will work with the source
code from a past assignment that implements a simple simulation of a warehouse. We also have some associated unit tests
and we will be exploring that relationship a little.

## Ant

Ant is a command line program, but your installation of Eclipse likely has an Ant plugin already installed and this
plugin will use an in-built `ant` executable. If you choose to use this plugin, open the Ant panel by selecting `Ant` in
the `Show View` menu, under the `Window` toolbar menu. Click the `Add Buildfiles` button and navigate to your buildfile
(after you've created it), and the buildfile's targets will be listed in the panel. Double clicking a target will run
it.

If you chose to use Ant on the command line, first install it by following the instructions from the   [Ant
manual](http://ant.apache.org/manual/index.html). You will need to create a batch file that sets up the JAVA_HOME,
ANT_HOME and PATH environment variables. The file `antSetup.bat` is an example Windows batch script that does this. Once
you have installed Ant and configured your batch script, open a command window, run your `antSetup.bat` file, and then
type `ant` at the command prompt. You should see a failure message as follows indicating that the `ant` executable ran
but lacks a build file:

    Buildfile: build.xml does not exist! Build failed

You can actually supply any buildfile name you wish, but people seldom do this, so we will create a `build.xml` file of
our own from scratch, and use it to control the compilation, testing and deployment of the application.

*Note: The build file instruction steps refer to running Ant on the command line. The Eclipse Ant plugin provides a
graphical interface over the `ant` executable, and will allow you to perform the same functions (i.e. running various
targets in your build file).*

## Project structure

We are going to use a slightly different directory structure from the usual Eclipse project. We will separate the unit
test code from the code for the model classes which will be slightly more professional. But we will need to be very
careful as there are significant advantages to maintaining the same package structure for the test classes as for the
model.

The source code for this practical is contained within subdirectories of the `src` folder of the
[repository](https://bitbucket.org/cab302/prac09-int-build), as you have come to expect from an Eclipse project. We now
assume that we need to build the project repeatedly, and in a professional environment. We first work to produce a
directory structure that matches the package structure, and thence to achieve a build file that handles both the
compilation of the model and test files, along with the execution of the unit tests themselves.

For these instructions, a directory called `Exercise` was created and will be used to refer to the top level project
directory. You may create your own, or use the directory that you have cloned/downloaded these instructions and the
provided `src` directory to. Within it, ensure the existence of both source and unit test directories which will
ultimately contain the appropriate `.java` files:

    C:\...\Exercise\src
    C:\...\Exercise\utest

Now examine the provided source code. In the `asgn1Solution` directory you will see that the first line of each of the
source files specifies package membership as shown:

    package asgn1Solution;

Note that this is true for both application code and tests. Create a directory called `asgn1Solution` in both the `src`
and `utest` directories, placing the application files and the tests in their appropriate location.

Repeat these tasks for the `asgn1Question` directory, again placing the files in the appropriate locations. You should
see this time that there are no unit tests to be considered and so no directory of this name is needed in the `utest`
hierarchy.

By now we should have:

    C:\...\Exercise\src\asgn1Question
    C:\...\Exercise\src\asgn1Solution
    C:\...\Exercise\utest\asgn1Solution

The root of the path will vary with your own installations. The key is that the source and unit test directories allow
the same package structure, but separate locations for the actual source and the unit test source, which are of course
very different things. After placing the source files and unit test files in their appropriate locations, we have:

![](imgs/0.png)

![](imgs/1.png)

And the unit tests here:

![](imgs/2.png)

The directory structures therefore match, and as you will recall, when writing Java, the directory structure is chosen
to match the package structure.

## Build file

Now change to the top directory of `Exercise`, which at this point should consist of the `src` and `utest` directory
entries. Using an editor of your choice (preferably one that performs XML syntax highlighting), create a file called
`build.xml` with the elements and attributes described below. You should run ant after you've completed each significant
edit, as this will confirm that the syntax is OK. You will find it easier to close each tag as it is opened, thus
maintaining syntactically correct XML.

Adjust any paths as needed in the instructions below.

The required steps are as follows:

1. In the `build.xml` file, create an ant `<project>` called `exercise`, with a default target of `build`, and a basedir
of `.`.

2. Create a property called `base`, which I have set to `artifacts`, meaning that it will sit below the basedir
specified above. This is a working target directory. At this point you should also create a working directory for test
output – `testBase`, set to `testFiles`, and a deployment directory for the final output from the build. The deployment
directory is completely up to you, but one example is given here to use as a model for all property setting:

```
<property name="deploy" value="D:/ExerciseDeployment" />
```

3. Create two more properties to specify the location of the JUnit jar files.

```
<property name="junitJar"
    value="H:/programs/junit/junit-4.11.jar" />
<property name="hamcrestJar"
    value="H:/programs/junit/hamcrest-core-1.3.jar" />
```

4. Create a target called `prepare`, specifying its description attribute as `"Create target directories"`. Within the
target element (i.e. before the closing tag), insert `mkdir` tasks to create the deployment directory, a directory for
the unit test reports, the `base` directory, the JavaDoc directory, and a directory for the unit test class files. In
the latter case, the syntax is as follows:

```
<mkdir dir="utest/classes" />
```

5. Now create a new target to compile the java source files. You should specify the path as shown, but you need not
specify the source files explicitly. My own practice is to add source files package by package to this command. This is
a balance between automation and actually understanding what is going on. The syntax will look something like this:

```
<target name="compile" depends="prepare"
    description="Compile source" >
  <javac srcdir="src" classpath="${base}" destdir="${base}"
      includeantruntime="false">
    <include name="asgn1Question/*.java" />
    <include name="asgn1Solution/*.java" />
  </javac>
</target>
```

Note the dependency on the `prepare` target, and the use of the Linux-style forward slashes for the directory. Use these
regardless of the system, windows or otherwise. The `classpath` attribute is necessary so that the compiler can find
imports in the packages of the current system. Remember that Java places classfiles by default within a directory
structure mirroring the packages. Class files will thus appear (based on the `destdir` setting) in directories under
`${base}`. Always use the `include` element as shown -- it allows convenient control of the files in the build. The
`includeantruntime` attribute is optional but setting it to false eliminates a warning about it not being set. This
attribute is used to indicate if your code makes use of the ant runtime classes.

6. Now create targets to handle the unit tests. The first is to compile the unit test source, and should largely mimic
`compile`. We then have to run the unit tests using a `junit` task. Copy and paste the compile target and use it as a
basis for the new target, which we will call `compileTests`. Its dependency is `compile` and the `srcdir` is plainly
`utest`, and the `destdir` `utest/classes`.

7. The critical point is to update the `classpath` to reference the JUnit jar files. Your target should thus look
something like this:

```
<target name="compileTests" depends="compile"
    description="Compile unit test source">
  <!-- Compile unit test source -->
  <javac srcdir="utest" classpath="${base}:${junitJar}:${hamcrestJar}"
      destdir="utest/classes" includeantruntime="false">
    <include name="asgn1Solution/*.java" />
  </javac>
</target>
```

8. Run ant at the command line with `ant compileTests` which should cause all of the targets to be executed, and thus
the compilation of the model and the test files. Do not proceed until you get output something like that seen below.
Note that ant will be pretty silent if some tasks have already been executed, and it may be very silent if you specify
compilation of a directory of source files that simply doesn't exist. So, your results may vary somewhat, but take a
good look at the resulting structures and see what you get.

![](imgs/3.png)

9. The `junit` test runner task is tricky, so I will provide it in full here and explain it:

```
<target name="utest" depends="compileTests"
    description="Run JUnit">
  <junit printsummary="true" failureproperty="junit.failure">
    <classpath>
      <pathelement path="utest/classes:${base}" />
      <pathelement path="${junitJar}:${hamcrestJar}" />
    </classpath>
    <batchtest todir="${testBase}">
      <fileset dir="utest/classes"/>
      <formatter type="xml"/>
    </batchtest>
  </junit>
</target>
```

10. The key issues for successful testing are the dependency on `compileTests`, the specification of the `batchtest`
element, which runs all of the test classes available (as specified by the `fileset` element), and the use of an
XML-based test result formatter. Note the four elements of the `classpath`, separated by `':'` characters. However, note
the critical `failureproperty`, which allows us to specify the behaviour when the tests fail. We shall come to this
below.

11. Invoke ant with the command line `ant utest` which should execute two test classes successfully. Take note of the
failure counts and error counts.

12. We are now almost complete, but there are a couple of things to do. Have a look in the `${testBase}` directory and
you will find a couple of xml files containing the results from the `junit` run. Browse them and get a sense of their
structure. They are useful enough, but we will regard them as raw output. We can do better, which brings us to the
`junitreport` element. Begin by adding one more item to the prepare task, making a directory called `utest/report`. Then
insert the following lines below the `junit` task, but within the `utest` target:

    <junitreport todir="utest/report">
      <fileset dir="${testBase}"/>
      <report todir="utest/report"/>
    </junitreport>

13. Invoke ant as before with the target `utest`. The output should be something like this:

![](imgs/4.png)

Browse the html-based report in the `utest/report` directory. Now add in the final line of the `utest` target, which
invokes the error facilities of JUnit and thus of ant:

    <fail if="junit.failure" message="junit tests failed - aborting"/>

14. Open up the test class `LedgerTest.java`, and change one of the parameters in a specified test, causing it to fail.
What is the behaviour of ant in these circumstances?

15. Finally, we are going to add two targets to the build file: one, called `build`, will pull everything together by
producing a jar from the completed code. This will become the default target, and the chain of dependencies will mean
that it cannot proceed without all the other tasks working. For now, leave the error in the `LedgerTest` class. The
`jar` task is very straightforward, but we will do a basic manifest as well -- a specification of those things which are
placed in the file. The `jar` task may be specified as follows:

    <target name="build" depends="utest" >
      <jar destfile="hiring.jar" basedir="${base}">
        <manifest>
          <!-- Who is building this jar? -->
          <attribute name="Built-By" value="${user.name}"/>
          <!-- Information about the program itself -->
          <attribute name="Implementation-Vendor" value="QUT"/>
          <attribute name="Implementation-Title" value="INBN370 Exercise"/>
          <attribute name="Implementation-Version" value="1.0.0"/>
        </manifest>
      </jar>
    </target>

16. Run it before fixing the error in the `LedgerTest` class, and we see that the test causes the whole build to fail,
and the jar is not produced. Fix the error, and the build completes successfully.

17. You should now introduce a few more targets to finish. The first, which will depend on `build`, will be called
`deploy`, and will involve creating a directory somewhere on the system containing a jar of the executable code and a
batch file to enable you to execute it. This is common practice for java apps in order to avoid the need to type huge
command lines. If you are struggling to specify an appropriate run command in java, try appropriate entries here:

```
java –classpath <jarname> <package.className>
```

and place this inside (say) `WarehouseSimulation.bat` in the exercise directory. Then write the `deploy` target, which
simply copies this batch file and `hiring.jar` to the deployment directory you specified earlier. You now have a very
good, well structured build file. What is the chain of dependencies for build and deploy?

18. It remains only to allow the developers to `clean` the directories, and to produce a copy of the source with
`javadoc`. Both of these tasks are straightforward based on the ones you have completed. What are the dependencies
required for the source target? Where should we send the generated JavaDoc?
