<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <link rel="stylesheet" href="imgs/kevinburke.markdown.css" type="text/css" />
</head>
<body>
<h1 id="cab302-software-development">CAB302 Software Development</h1>
<h1 id="practical-9-the-integrated-build-process">Practical 9: The Integrated Build Process</h1>
<p>The classes for this week are about integrated builds in the modern development environment. Build systems like Ant allow convenient automation of processes such as compilation, testing, documentation generation, and deployment. We begin slowly, but build up to generating a professional build file for some existing code. We will work with the source code from a past assignment that implements a simple simulation of a warehouse. We also have some associated unit tests and we will be exploring that relationship a little.</p>
<h2 id="ant">Ant</h2>
<p>Ant is a command line program, but your installation of Eclipse likely has an Ant plugin already installed and this plugin will use an in-built <code>ant</code> executable. If you choose to use this plugin, open the Ant panel by selecting <code>Ant</code> in the <code>Show View</code> menu, under the <code>Window</code> toolbar menu. Click the <code>Add Buildfiles</code> button and navigate to your buildfile (after you've created it), and the buildfile's targets will be listed in the panel. Double clicking a target will run it.</p>
<p>If you chose to use Ant on the command line, first install it by following the instructions from the <a href="http://ant.apache.org/manual/index.html">Ant manual</a>. You will need to create a batch file that sets up the JAVA_HOME, ANT_HOME and PATH environment variables. The file <code>antSetup.bat</code> is an example Windows batch script that does this. Once you have installed Ant and configured your batch script, open a command window, run your <code>antSetup.bat</code> file, and then type <code>ant</code> at the command prompt. You should see a failure message as follows indicating that the <code>ant</code> executable ran but lacks a build file:</p>
<pre><code>Buildfile: build.xml does not exist! Build failed</code></pre>
<p>You can actually supply any buildfile name you wish, but people seldom do this, so we will create a <code>build.xml</code> file of our own from scratch, and use it to control the compilation, testing and deployment of the application.</p>
<p><em>Note: The build file instruction steps refer to running Ant on the command line. The Eclipse Ant plugin provides a graphical interface over the <code>ant</code> executable, and will allow you to perform the same functions (i.e. running various targets in your build file).</em></p>
<h2 id="project-structure">Project structure</h2>
<p>We are going to use a slightly different directory structure from the usual Eclipse project. We will separate the unit test code from the code for the model classes which will be slightly more professional. But we will need to be very careful as there are significant advantages to maintaining the same package structure for the test classes as for the model.</p>
<p>The source code for this practical is contained within subdirectories of the <code>src</code> folder of the <a href="https://bitbucket.org/cab302/prac09-int-build">repository</a>, as you have come to expect from an Eclipse project. We now assume that we need to build the project repeatedly, and in a professional environment. We first work to produce a directory structure that matches the package structure, and thence to achieve a build file that handles both the compilation of the model and test files, along with the execution of the unit tests themselves.</p>
<p>For these instructions, a directory called <code>Exercise</code> was created and will be used to refer to the top level project directory. You may create your own, or use the directory that you have cloned/downloaded these instructions and the provided <code>src</code> directory to. Within it, ensure the existence of both source and unit test directories which will ultimately contain the appropriate <code>.java</code> files:</p>
<pre><code>C:\...\Exercise\src
C:\...\Exercise\utest</code></pre>
<p>Now examine the provided source code. In the <code>asgn1Solution</code> directory you will see that the first line of each of the source files specifies package membership as shown:</p>
<pre><code>package asgn1Solution;</code></pre>
<p>Note that this is true for both application code and tests. Create a directory called <code>asgn1Solution</code> in both the <code>src</code> and <code>utest</code> directories, placing the application files and the tests in their appropriate location.</p>
<p>Repeat these tasks for the <code>asgn1Question</code> directory, again placing the files in the appropriate locations. You should see this time that there are no unit tests to be considered and so no directory of this name is needed in the <code>utest</code> hierarchy.</p>
<p>By now we should have:</p>
<pre><code>C:\...\Exercise\src\asgn1Question
C:\...\Exercise\src\asgn1Solution
C:\...\Exercise\utest\asgn1Solution</code></pre>
<p>The root of the path will vary with your own installations. The key is that the source and unit test directories allow the same package structure, but separate locations for the actual source and the unit test source, which are of course very different things. After placing the source files and unit test files in their appropriate locations, we have:</p>
<div class="figure">
<img src="imgs/0.png" />
</div>
<div class="figure">
<img src="imgs/1.png" />
</div>
<p>And the unit tests here:</p>
<div class="figure">
<img src="imgs/2.png" />
</div>
<p>The directory structures therefore match, and as you will recall, when writing Java, the directory structure is chosen to match the package structure.</p>
<h2 id="build-file">Build file</h2>
<p>Now change to the top directory of <code>Exercise</code>, which at this point should consist of the <code>src</code> and <code>utest</code> directory entries. Using an editor of your choice (preferably one that performs XML syntax highlighting), create a file called <code>build.xml</code> with the elements and attributes described below. You should run ant after you've completed each significant edit, as this will confirm that the syntax is OK. You will find it easier to close each tag as it is opened, thus maintaining syntactically correct XML.</p>
<p>Adjust any paths as needed in the instructions below.</p>
<p>The required steps are as follows:</p>
<ol style="list-style-type: decimal">
<li><p>In the <code>build.xml</code> file, create an ant <code>&lt;project&gt;</code> called <code>exercise</code>, with a default target of <code>build</code>, and a basedir of <code>.</code>.</p></li>
<li><p>Create a property called <code>base</code>, which I have set to <code>artifacts</code>, meaning that it will sit below the basedir specified above. This is a working target directory. At this point you should also create a working directory for test output – <code>testBase</code>, set to <code>testFiles</code>, and a deployment directory for the final output from the build. The deployment directory is completely up to you, but one example is given here to use as a model for all property setting:</p></li>
</ol>
<pre><code>&lt;property name=&quot;deploy&quot; value=&quot;D:/ExerciseDeployment&quot; /&gt;</code></pre>
<ol start="3" style="list-style-type: decimal">
<li>Create two more properties to specify the location of the JUnit jar files.</li>
</ol>
<pre><code>&lt;property name=&quot;junitJar&quot;
    value=&quot;H:/programs/junit/junit-4.11.jar&quot; /&gt;
&lt;property name=&quot;hamcrestJar&quot;
    value=&quot;H:/programs/junit/hamcrest-core-1.3.jar&quot; /&gt;</code></pre>
<ol start="4" style="list-style-type: decimal">
<li>Create a target called <code>prepare</code>, specifying its description attribute as <code>&quot;Create target directories&quot;</code>. Within the target element (i.e. before the closing tag), insert <code>mkdir</code> tasks to create the deployment directory, a directory for the unit test reports, the <code>base</code> directory, the JavaDoc directory, and a directory for the unit test class files. In the latter case, the syntax is as follows:</li>
</ol>
<pre><code>&lt;mkdir dir=&quot;utest/classes&quot; /&gt;</code></pre>
<ol start="5" style="list-style-type: decimal">
<li>Now create a new target to compile the java source files. You should specify the path as shown, but you need not specify the source files explicitly. My own practice is to add source files package by package to this command. This is a balance between automation and actually understanding what is going on. The syntax will look something like this:</li>
</ol>
<pre><code>&lt;target name=&quot;compile&quot; depends=&quot;prepare&quot;
    description=&quot;Compile source&quot; &gt;
  &lt;javac srcdir=&quot;src&quot; classpath=&quot;${base}&quot; destdir=&quot;${base}&quot;
      includeantruntime=&quot;false&quot;&gt;
    &lt;include name=&quot;asgn1Question/*.java&quot; /&gt;
    &lt;include name=&quot;asgn1Solution/*.java&quot; /&gt;
  &lt;/javac&gt;
&lt;/target&gt;</code></pre>
<p>Note the dependency on the <code>prepare</code> target, and the use of the Linux-style forward slashes for the directory. Use these regardless of the system, windows or otherwise. The <code>classpath</code> attribute is necessary so that the compiler can find imports in the packages of the current system. Remember that Java places classfiles by default within a directory structure mirroring the packages. Class files will thus appear (based on the <code>destdir</code> setting) in directories under <code>${base}</code>. Always use the <code>include</code> element as shown -- it allows convenient control of the files in the build. The <code>includeantruntime</code> attribute is optional but setting it to false eliminates a warning about it not being set. This attribute is used to indicate if your code makes use of the ant runtime classes.</p>
<ol start="6" style="list-style-type: decimal">
<li><p>Now create targets to handle the unit tests. The first is to compile the unit test source, and should largely mimic <code>compile</code>. We then have to run the unit tests using a <code>junit</code> task. Copy and paste the compile target and use it as a basis for the new target, which we will call <code>compileTests</code>. Its dependency is <code>compile</code> and the <code>srcdir</code> is plainly <code>utest</code>, and the <code>destdir</code> <code>utest/classes</code>.</p></li>
<li><p>The critical point is to update the <code>classpath</code> to reference the JUnit jar files. Your target should thus look something like this:</p></li>
</ol>
<pre><code>&lt;target name=&quot;compileTests&quot; depends=&quot;compile&quot;
    description=&quot;Compile unit test source&quot;&gt;
  &lt;!-- Compile unit test source --&gt;
  &lt;javac srcdir=&quot;utest&quot; classpath=&quot;${base}:${junitJar}:${hamcrestJar}&quot;
      destdir=&quot;utest/classes&quot; includeantruntime=&quot;false&quot;&gt;
    &lt;include name=&quot;asgn1Solution/*.java&quot; /&gt;
  &lt;/javac&gt;
&lt;/target&gt;</code></pre>
<ol start="8" style="list-style-type: decimal">
<li>Run ant at the command line with <code>ant compileTests</code> which should cause all of the targets to be executed, and thus the compilation of the model and the test files. Do not proceed until you get output something like that seen below. Note that ant will be pretty silent if some tasks have already been executed, and it may be very silent if you specify compilation of a directory of source files that simply doesn't exist. So, your results may vary somewhat, but take a good look at the resulting structures and see what you get.</li>
</ol>
<div class="figure">
<img src="imgs/3.png" />
</div>
<ol start="9" style="list-style-type: decimal">
<li>The <code>junit</code> test runner task is tricky, so I will provide it in full here and explain it:</li>
</ol>
<pre><code>&lt;target name=&quot;utest&quot; depends=&quot;compileTests&quot;
    description=&quot;Run JUnit&quot;&gt;
  &lt;junit printsummary=&quot;true&quot; failureproperty=&quot;junit.failure&quot;&gt;
    &lt;classpath&gt;
      &lt;pathelement path=&quot;utest/classes:${base}&quot; /&gt;
      &lt;pathelement path=&quot;${junitJar}:${hamcrestJar}&quot; /&gt;
    &lt;/classpath&gt;
    &lt;batchtest todir=&quot;${testBase}&quot;&gt;
      &lt;fileset dir=&quot;utest/classes&quot;/&gt;
      &lt;formatter type=&quot;xml&quot;/&gt;
    &lt;/batchtest&gt;
  &lt;/junit&gt;
&lt;/target&gt;</code></pre>
<ol start="10" style="list-style-type: decimal">
<li><p>The key issues for successful testing are the dependency on <code>compileTests</code>, the specification of the <code>batchtest</code> element, which runs all of the test classes available (as specified by the <code>fileset</code> element), and the use of an XML-based test result formatter. Note the four elements of the <code>classpath</code>, separated by <code>':'</code> characters. However, note the critical <code>failureproperty</code>, which allows us to specify the behaviour when the tests fail. We shall come to this below.</p></li>
<li><p>Invoke ant with the command line <code>ant utest</code> which should execute two test classes successfully. Take note of the failure counts and error counts.</p></li>
<li><p>We are now almost complete, but there are a couple of things to do. Have a look in the <code>${testBase}</code> directory and you will find a couple of xml files containing the results from the <code>junit</code> run. Browse them and get a sense of their structure. They are useful enough, but we will regard them as raw output. We can do better, which brings us to the <code>junitreport</code> element. Begin by adding one more item to the prepare task, making a directory called <code>utest/report</code>. Then insert the following lines below the <code>junit</code> task, but within the <code>utest</code> target:</p>
<p><junitreport todir="utest/report"> <fileset dir="${testBase}"/> <report todir="utest/report"/> </junitreport></p></li>
<li><p>Invoke ant as before with the target <code>utest</code>. The output should be something like this:</p></li>
</ol>
<div class="figure">
<img src="imgs/4.png" />
</div>
<p>Browse the html-based report in the <code>utest/report</code> directory. Now add in the final line of the <code>utest</code> target, which invokes the error facilities of JUnit and thus of ant:</p>
<pre><code>&lt;fail if=&quot;junit.failure&quot; message=&quot;junit tests failed - aborting&quot;/&gt;</code></pre>
<ol start="14" style="list-style-type: decimal">
<li><p>Open up the test class <code>LedgerTest.java</code>, and change one of the parameters in a specified test, causing it to fail. What is the behaviour of ant in these circumstances?</p></li>
<li><p>Finally, we are going to add two targets to the build file: one, called <code>build</code>, will pull everything together by producing a jar from the completed code. This will become the default target, and the chain of dependencies will mean that it cannot proceed without all the other tasks working. For now, leave the error in the <code>LedgerTest</code> class. The <code>jar</code> task is very straightforward, but we will do a basic manifest as well -- a specification of those things which are placed in the file. The <code>jar</code> task may be specified as follows:</p>
<p><target name="build" depends="utest" > <jar destfile="hiring.jar" basedir="${base}"> <manifest> <!-- Who is building this jar? --> <attribute name="Built-By" value="${user.name}"/> <!-- Information about the program itself --> <attribute name="Implementation-Vendor" value="QUT"/> <attribute name="Implementation-Title" value="INBN370 Exercise"/> <attribute name="Implementation-Version" value="1.0.0"/> </manifest> </jar> </target></p></li>
<li><p>Run it before fixing the error in the <code>LedgerTest</code> class, and we see that the test causes the whole build to fail, and the jar is not produced. Fix the error, and the build completes successfully.</p></li>
<li><p>You should now introduce a few more targets to finish. The first, which will depend on <code>build</code>, will be called <code>deploy</code>, and will involve creating a directory somewhere on the system containing a jar of the executable code and a batch file to enable you to execute it. This is common practice for java apps in order to avoid the need to type huge command lines. If you are struggling to specify an appropriate run command in java, try appropriate entries here:</p></li>
</ol>
<pre><code>java –classpath &lt;jarname&gt; &lt;package.className&gt;</code></pre>
<p>and place this inside (say) <code>WarehouseSimulation.bat</code> in the exercise directory. Then write the <code>deploy</code> target, which simply copies this batch file and <code>hiring.jar</code> to the deployment directory you specified earlier. You now have a very good, well structured build file. What is the chain of dependencies for build and deploy?</p>
<ol start="18" style="list-style-type: decimal">
<li>It remains only to allow the developers to <code>clean</code> the directories, and to produce a copy of the source with <code>javadoc</code>. Both of these tasks are straightforward based on the ones you have completed. What are the dependencies required for the source target? Where should we send the generated JavaDoc?</li>
</ol>
</body>
</html>
